# Template Zapf (GUTenberg)



## Source of the content

Hermann Zapf, « Typographie des caractères romains de la Renaissance », *Cahiers GUTenberg*, n<sup>o</sup> 37-38 (2000), p. 44-52. URL: http://cahiers.gutenberg.eu.org/fitem?id=CG_2000___37-38_44_0

HTML structuration by Julie Blanc

## Instructions

- Use any local server.
- Write your content in `index.html`
- Add your style in `style-print.css`
- If you need to register handlers, you can do it in `jshandlers.js`
- To have the interface bar at the bottom of the page, add the class `interface-bar-bottom` to the body of your index.html

## Scripts and template included

- Paged.js 0.4.1
- `reload-in-place.js` v1.3 by Nicolas Taffin and Sameh Chafik
- Original template: https://gitlab.coko.foundation/pagedjs/starter-kits/book_avanced-interface


## Licence

MIT License https://opensource.org/licenses/MIT
Developped by Julie Blanc ([julie-blanc.fr](https://julie-blanc.fr/en/))



---

Link of this repo: https://gitlab.com/pagedjs-resources/workshops/template-gutenberg_zapf

